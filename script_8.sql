#1
SELECT maker, (SELECT COUNT(*) FROM product WHERE maker = p1.maker AND type =  'PC') as pc, 
(SELECT COUNT(*) FROM product WHERE maker = p1.maker AND type =  'Laptop') as laptop, 
(SELECT COUNT(*) FROM product WHERE maker = p1.maker AND type =  'Printer') as printer FROM product p1
GROUP BY maker;
#2
select maker, avg(screen) as "Середній розмір екрану"
from Product P join Laptop L on L.model = P.model
group by maker;
#3
select maker, max(price) as "Максимальна ціна ПК"
from Product P join PC pc on P.model = pc.model
group by maker;
#4
select maker, min(price) as "Мінімальна ціна ПК"
from Product P join PC pc on P.model = pc.model
group by maker;
#5
select speed, avg(price) as "Середня ціна"
from PC
group by speed 
having speed > 600;
#6
select maker, avg(hd) as "Середній розмір жорсткого диску"
from Product P join PC pc on P.model = pc.model
where maker in (select maker from Product P2 where `type` = "printer")
group by maker;
#7
select ship, displacement, numGuns from Classes C join Ships S on C.class = S.class 
join Outcomes O on O.ship = S.name 
where battle = 'Guadalcanal';
#8
select ship, displacement, numGuns from Classes C join Ships S on C.class = S.class 
join Outcomes O on O.ship = S.name 
where result = 'damaged';

