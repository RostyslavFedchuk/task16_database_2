#1
select maker from Product where `type` = "PC" and maker not in (select maker from Product where type = 'Laptop');
#2
select maker from Product where `type` = "PC" and maker!=all(select maker from Product where type = 'Laptop');
#3
select maker from Product where `type` = "PC" and maker!=any(select maker from Product where type = 'Laptop');
#4
select maker from Product where `type` = "PC" and maker in (select maker from Product where type = 'Laptop');
#5
select maker from Product where `type` = "PC" and maker=all(select maker from Product where type = 'Laptop');
#6
select maker from Product where `type` = "PC" and maker = any(select maker from Product where type = 'Laptop');
#7
select maker from Product where `type` = "PC" and model in (select model from PC);
select maker from Product where `type` = "PC" and model !=ALL (select model from PC);
select maker from Product where `type` = "PC" and model = ANY (select model from PC);
#8
select country, class from Classes where country = ANY(select country from classes where country = 'Ukraine') OR TRUE;
#9
select ship, battle, `date` from outcomes O join battles b on b.name = battle
where result = "damaged" and ship in( select ship from outcomes o join battles b2 on o.battle = b2.name where date > b.date);
#10
select count(model) as Count_PC from Product where maker = "A" and `type`="PC" and model is not null;
#11
select distinct maker from Product where `type` = "PC" and model not in(select model from PC);
#12
select model, price from Laptop where price > ALL(select price from PC);