#1
SELECT maker FROM product WHERE NOT EXISTS (SELECT model FROM product WHERE maker = product.maker AND model NOT IN (SELECT model FROM pc));
#2
SELECT maker FROM product WHERE EXISTS (SELECT model FROM pc WHERE model = product.model AND speed >= 750);
#3
SELECT maker FROM product p WHERE EXISTS (SELECT model FROM pc WHERE model = p.model AND speed >= 750)
AND maker IN (SELECT maker FROM product p1 WHERE EXISTS (SELECT model FROM laptop WHERE model = p1.model AND speed >= 750));
#4
select maker from product p join pc on p.model = pc.model and speed = (SELECT MAX(speed) FROM pc) and exists(select maker from printer);
#5
select name, launched, displacement from ships s join classes c on s.class = c.class where launched>=1922 and displacement > 35000;
#6
select class from classes where exists (select class from ships join outcomes on name = ship or class = ship and result ="sunk");
#7
select distinct maker from product p where `type` = "laptop" and exists (select maker from product p1 where `type` = "printer" and p.maker = p1.maker);
#8
select distinct maker from product p where `type` = "laptop" and not exists (select maker from product p1 where `type` = "printer" and p.maker = p1.maker);