#1
select maker, P1.model, P1.`type`, price from Product P1 join Laptop L on P1.model = L.model where maker = "B"
union select maker, P2.model, P2.`type`, price from Product P2 join PC on P2.model = PC.model where maker = "B"
union select maker, P3.model, P3.`type`, price from Product P3 join printer pr on P3.model = pr.model where maker = "B";
#2
select P1.`type`, P1.model, price as "максимальна ціна" from Product P1 join Laptop L on P1.model = L.model group by P1.model 
having price = (select max(price) from Product P2 join Laptop L2 on P2.model = L2.model where P2.model = P1.model)
union 
select P3.`type`, P3.model, price from Product P3 join PC pc1 on P3.model = pc1.model group by P3.model 
having price = (select max(price) from Product P4 join PC pc2 on P4.model = pc2.model where P4.model = P3.model)
union 
select P5.`type`, P5.model, price from Product P5 join Printer pr1 on P5.model = pr1.model group by P5.model 
having price = (select max(price) from Product P6 join Printer pr2 on P6.model = pr2.model where P6.model = P5.model);
#3
select maker,`type`, avg(price) as "Загальна середня ціна" from Product P1 join Laptop L on P1.model = L.model where maker = "A"
union select maker, `type`, avg(price) from Product P2 join PC on P2.model = pc.model where maker = "A";
#8
select name from ships where launched < 1942 ;
