#1
select model, price from printer where price = (Select max(price) from printer);
#2
select `type`, l.model, speed from laptop l join product p on l.model = p.model 
where speed < ALL( select speed from PC);
#3
select maker, price from product p join printer pr on p.model = pr.model
where price = (Select min(price) from  printer where color="y");
#4
select maker, count(model) as Model_count from product where `type` = "PC" group by maker having count(model) >=2;
#5
select maker, avg(hd) from pc join product p on pc.model = p.model
where type="PC" group by maker having maker = ANY(Select maker from product where `type` = "printer");
#6
select `date`, count(*) as "Число рейсів" from Pass_in_trip PIT join Trip T on PIT.trip_no = T.trip_no
where town_from="London" group by `date`;
#7
select `date`, count(*) as "Число рейсів" from Pass_in_trip PIT join Trip T on PIT.trip_no = T.trip_no
where town_to = 'Moscow'
group by `date`
having count(*) = (select max(total) from (Select count(*) as total from Pass_in_trip PIT join Trip T 
on PIT.trip_no = T.trip_no where town_to = 'Moscow' GROUP BY `date`) as tmp);
#8
select country, count(*) as "Число кораблів", launched from Classes c join Ships s on c.class = s.class
group by country
having launched = (select l from (Select launched l, count(*) total from Classes c1 join Ships s1 on c1.class = s1.class
	where c1.country = c.country GROUP BY launched) tmp order by total desc limit 1);
