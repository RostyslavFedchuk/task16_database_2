#1
select avg(price), concat("середня ціна = ", avg(price)) as AVG_PRICE from laptop;
#2
select concat("Код - ", `code`) as code, concat("Модель - ", `model`) as model, concat("Швидкість - ", `speed`) as speed, 
concat("Оперативка - ", `ram`) as ram, concat("Об'єм диску - ", `hd`) as hd, concat("cd - ", `cd`) as cd, 
concat("Ціна - ", `price`) as price from PC;
#3
select `code`, `point`, concat(year(`date`),".",month(`date`),".",day(`date`)) as Date, inc from income;
#4
select ship, battle, case when result = "damaged" then "Пошкоджений"when result = "OK" then "Все файно"
when result = "sunk" then "Потоплений" end as Результат from outcomes;
#5
select trip_no, date, ID_psg, CONCAT("Ряд: ", SUBSTRING(place,1,1), "; місце: ",SUBSTRING(place,2)) as Місце from Pass_in_trip ;
#6
select ID_comp, plane, concat("From ",town_from, " to ", town_to) as Direction, time_out, time_in from Trip;
